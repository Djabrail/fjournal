export const state = () => ({
    list: [],
    item: null
})

export const mutations = {
    SET_NEWS(state, data) {
        state.list = data
    },
    SET_NEWS_ITEM(state, data) {
        state.item = data[0]
    }
}

export const actions = {
    async getNews({ commit }) {
        const news = await this.$axios.$get('news')
        commit('SET_NEWS', news)
    },
    async getNewsCategory({ commit }, category) {
        const news = await this.$axios.$get(`news?category=${category}`)
        commit('SET_NEWS', news)
    },
    async getNewsArticle({ commit }, slug) {
        const item = await this.$axios.$get(`news?slug=${slug}`)
        commit('SET_NEWS_ITEM', item)
    }
};

export const getters = {
};